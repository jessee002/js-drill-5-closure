
function cacheFunction(cb){

  if(arguments.length === 0 || typeof arguments[0] !== 'function'){
    throw new Error(`proper function or number not provided`)
  }
    let cache = {};
  
    function invokecallFunction(...args){
        if(cache[args] !== undefined){
          return cache[args];
        }else{
         cache[args] = cb(...args);
          return cache[args];
        } 
   }
    return invokecallFunction;
}
module.exports = cacheFunction;
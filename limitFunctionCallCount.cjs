
function limitFunctionCallCount(cb, n)
{
    if(!n || arguments.length === 0 || typeof arguments[0] !== 'function' || typeof n !== 'number'){
        throw new Error(`proper function or number not provided`)
    }

    let val = 0;
    function invokeFunc(...args)
    {
        val++;
        if(val <=n ) return cb(...args); 
        else return null;

        // invokeFunc();
    }    
    return invokeFunc;
}
module.exports = limitFunctionCallCount;
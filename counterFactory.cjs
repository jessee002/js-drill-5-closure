function counterFactory(){
 
let counter = 0;

 function increment(){
     counter+=1;
     return counter;
  }

 function decrement(){
     counter-=1; 
     return counter  //console.log(counter);
   }

 const obj = {increment, decrement};

  return obj;
}
module.exports = counterFactory;